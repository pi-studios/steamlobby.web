﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SteamLobby.Web.Middleware
{
    public class ResponsiveLogger
    {
        private Stopwatch ServerExecutionStopwatch { get; } = new Stopwatch();
        
        public long ServerExecutionTime { get; private set; }

        public void BeginServerExecution()
        {
            this.ServerExecutionStopwatch.Start();
        }

        public void EndServerExecution()
        {
            this.ServerExecutionStopwatch.Stop();
            this.ServerExecutionTime = ServerExecutionStopwatch.ElapsedMilliseconds;
            this.ServerExecutionStopwatch.Reset();
        }
    }
}

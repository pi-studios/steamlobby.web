﻿using Microsoft.AspNet.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using System.Diagnostics;

namespace SteamLobby.Web.Middleware
{

    public class ResponsiveLoggerMiddleware
    {
        private RequestDelegate Next { get; }

        public ResponsiveLoggerMiddleware(RequestDelegate next)
        {
            this.Next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            ResponsiveLogger responsiveLogger = new ResponsiveLogger();
            responsiveLogger.BeginServerExecution();
            context.SetFeature(responsiveLogger);
            await Next(context);
            responsiveLogger.EndServerExecution();

            context.Response.Headers.Append("X-ElapsedTime", responsiveLogger.ServerExecutionTime.ToString());
            var isHtml = context.Response.ContentType?.ToLower().Contains("text/html");
            if (context.Response.StatusCode == 200 && isHtml.GetValueOrDefault())
                await context.Response.WriteAsync(string.Format("<!-- Server Execution Time: {0} -->", responsiveLogger.ServerExecutionTime));
        }
    }

    public static class ApplicationBuilderExtentions
    {
        public static IApplicationBuilder UseResponsiveLogger(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ResponsiveLoggerMiddleware>();
        }
    }
}
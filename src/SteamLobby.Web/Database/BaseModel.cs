﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Framework.DependencyInjection;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SteamLobby.Web.Database
{
    public class BaseModel
    {
        [Key] [IgnoreCopy]
        public Guid Id { get; set; } = Guid.NewGuid();

        [NotMapped]
        internal object SafeCopy
        {
            get
            {
                return this;
            }
        }

        private DatabaseContext database;
        [Newtonsoft.Json.JsonIgnore]
        public DatabaseContext Database
        {
            get
            {
                if (database == null)
                    database = Application.Builder.ApplicationServices.GetRequiredService<DatabaseContext>();

                return database;
            }
        }
    }
}

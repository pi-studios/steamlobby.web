﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SteamLobby.Web.Database
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Models.Member> Members { get; set; }
        public DbSet<Models.Session> Sessions { get; set; }
        public DbSet<Models.PasswordHash> PasswordHashes { get; set; }

        public DbSet<Models.Team> Teams { get; set; }
        public DbSet<Models.Event> Events { get; set; }
        public DbSet<Models.Match> Matches { get; set; }

        //public Thread AutoSaveThread { get; private set; }

        protected override void OnConfiguring(EntityOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Application.Configuration.Get("db:master:connectionString"));
        }
    }
}

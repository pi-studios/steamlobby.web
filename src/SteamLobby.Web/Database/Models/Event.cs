﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace SteamLobby.Web.Database.Models
{
    public class Event : BaseModel
    {
        [ListTable(Display = "Name")]
        public string Name { get; set; }
        public DateTime? ScheduledStart { get; set; }
        public DateTime? ScheduledEnd { get; set; }
        [ListTable(Display = "Logo", Format = "<img class='circle' style='height: 32px;' src='{0}' />", Position = 1, Raw = true)]
        public string LogoUrl { get; set; }
        public string BannerUrl { get; set; }
        public string RawData { get; set; }
        public string TeamList { get; set; }

        public object GetData()
        {
            return JsonConvert.DeserializeObject<ExpandoObject>(this.RawData);
        }

        public void SetData(object data)
        {
            this.RawData = JsonConvert.SerializeObject(data);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Microsoft.AspNet.Mvc;

namespace SteamLobby.Web.Database.Models
{
    public class Member : BaseModel
    {
        [NotMapped]
        internal new object SafeCopy { get { return new { Id, ProfileId, ProfileName, ProfileAboutMe, ProfileBirthday, ProfileAvatarUrl, ProfileCountry, HeaderUrl, FirstName, LastName }; } }

        [ListTable(Display = "Email")]
        public string Email { get; set; }
        [JsonIgnore]
        public string Permissions
        {
            get
            {
                return string.Join(",", this.FormattedPermissions);
            }
            set
            {
                if (value != null)
                    this.FormattedPermissions = new List<string>(value.Split(','));
            }
        }

        public Guid TeamId { get; set; }

        #region Profile
        [ListTable(Display = "Profile Id", Position = 1)]
        public string ProfileId { get; set; }
        [ListTable(Display = "Name")]
        public string ProfileName { get; set; }
        public string ProfileAboutMe { get; set; }
        public DateTime ProfileBirthday { get; set; }
        [ListTable(Display = "Avatar", Format = "<img class='circle' style='height: 32px;' src='{0}' />", Position = 0, Raw = true)]
        public string ProfileAvatarUrl { get; set; }
        public string ProfileCountry { get; set; }
        public string HeaderUrl { get; set; }
        #endregion

        [NotMapped]
        public string FirstName { get { return ProfileName == null ? null : ProfileName.Split(' ').FirstOrDefault(); } }
        [NotMapped]
        public string LastName { get { return ProfileName == null ? null : ProfileName.Split(' ').Count() >= 2 ? ProfileName.Split(' ').LastOrDefault() : ""; } }

        [NotMapped]
        public List<string> FormattedPermissions { get; set; } = new List<string>();

        private Team team = null;
        public Team GetTeam()
        {
            if (team == null)
                team = Database.Teams.Where(e => e.Id == this.TeamId).FirstOrDefault();
            return team;
        }

        public bool HasPermission(string permission)
        {
            if (Application._NO_AUTH)
                return true;

            if (FormattedPermissions.Contains(permission))
                return true;

            string[] permissionNodes = permission.Split('.');

            for (int i = 0; i < permissionNodes.Length; i++)
            {
                string _permission = string.Join(".", permissionNodes, 0, i) + (i == 0 ? "*" : ".*");
                if (FormattedPermissions.Contains(_permission))
                    return true;
            }

            return false;
        }
    }
}

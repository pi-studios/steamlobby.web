﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.Framework.DependencyInjection;

namespace SteamLobby.Web.Database.Models
{
    public class Session : BaseModel
    {
        private static RandomNumberGenerator randomNumberGenerator = RandomNumberGenerator.Create();

        public Guid MemberId { get; set; }
        public string KeyIdentifier { get; set; }
        public string ApiIdentifier { get; set; }
        public string AddressIdentifier { get; set; }
        public string ClientIdentifier { get; set; }
        public DateTime Expiration { get; set; }

        public static Session New()
        {
            return new Session() { KeyIdentifier = randomNumberGenerator.GenerateHexString(64), Expiration = DateTime.UtcNow.AddMonths(1) };
        }
    }
}

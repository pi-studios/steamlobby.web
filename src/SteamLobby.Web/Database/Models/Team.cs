﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SteamLobby.Web.Database.Models
{
    public class Team : BaseModel
    {
        [NotMapped]
        internal new object SafeCopy { get { return new { Name, ShortName, PublicId, Game, Division, LogoUrl, HeaderUrl }; } }

        [ListTable(Display = "Name")]
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string PublicId { get; set; }
        [ListTable(Display = "Game", Format = "<img class='circle' style='height: 32px;' src='/images/icons/games/{0}.png' />", Position = 0, Raw = true)]
        public string Game { get; set; }
        [ListTable(Display = "Division")]
        public string Division { get; set; }
        [ListTable(Display = "Logo", Format = "<img class='circle' style='height: 32px;' src='{0}' />", Position = 1, Raw = true)]
        public string LogoUrl { get; set; }
        public string HeaderUrl { get; set; }

        [JsonIgnore] [NotMapped]
        public IEnumerable<Member> Members
        {
            get
            {
                if (this.Id == Guid.Empty)
                    return new Member[0];

                return Database.Members.Where(e => e.TeamId == this.Id);
            }
        }
    }
}

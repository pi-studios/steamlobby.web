﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SteamLobby.Web.Database.Models
{
    public class Match : BaseModel
    {
        public Guid HomeTeamId { get; set; }
        public Guid AwayTeamId { get; set; }
        public Guid EventId { get; set; }
        public DateTime? ScheduledStart { get; set; }
        public DateTime? Completed { get; set; }
        public MatchState? State { get; set; }
        public string RawData { get; set; }
        public string RawScoreData { get; set; }
        public bool? HomeTeamWin { get; set; }

        [NotMapped][JsonIgnore]
        public Team HomeTeam { get { return Database.Teams.Where(e => e.Id == this.HomeTeamId).FirstOrDefault(); } }
        [NotMapped][JsonIgnore]
        public Team AwayTeam { get { return Database.Teams.Where(e => e.Id == this.AwayTeamId).FirstOrDefault(); } }
        [NotMapped][JsonIgnore]
        public Event Event { get { return Database.Events.Where(e => e.Id == this.EventId).FirstOrDefault(); } }
    }

    public enum MatchState : byte
    {
        UPCOMING,
        LIVE,
        OT_LIVE,
        DELAYED,
        COMPLETE
    }
}

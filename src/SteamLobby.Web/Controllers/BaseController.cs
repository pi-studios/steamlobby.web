﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using System.Net;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
using Microsoft.Framework.Internal;
using Microsoft.AspNet.Http.Features;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SteamLobby.Web.Controllers
{
    public class BaseController : Controller
    {
        public RequestHelper RequestHelper { get; }
    
        public Database.DatabaseContext Database { get; }

        public string RemoteAddress
        {
            get
            {
                return Context.GetFeature<IHttpConnectionFeature>().RemoteIpAddress.ToString();
            }
        }

        public BaseController(RequestHelper requestHelper)
        {
            this.RequestHelper = requestHelper;
            this.Database = Application.Builder.ApplicationServices.GetRequiredService<Database.DatabaseContext>();
        }

        [NonAction]
        public IActionResult RequireAuthentication(Func<IActionResult> authenticationPass)
        {
            return RequireAuthentication(permission: null, authenticationPass: authenticationPass);
        }

        [NonAction]
        public IActionResult RequireAuthentication(string permission, Func<IActionResult> authenticationPass)
        {
            return RequireAuthentication(permission: permission, authenticationPass: authenticationPass, authenticationFail: () => { return View("signin"); });
        }

        [NonAction]
        public IActionResult RequireAuthentication(Func<IActionResult> authenticationPass, Func<IActionResult> authenticationFail)
        {
            return RequireAuthentication(permission: null, authenticationPass: authenticationPass, authenticationFail: authenticationFail);
        }

        [NonAction]
        public IActionResult RequireAuthentication(string permission, Func<IActionResult> authenticationPass, Func<IActionResult> authenticationFail)
        {
            if (RequestHelper.Member == null)
                return authenticationFail();
            else if (permission != null && !RequestHelper.Member.HasPermission(permission))
                return authenticationFail();

            return authenticationPass();
        }

        [NonAction]
        public IActionResult ApiSuccess(object response = null, string message = null)
        {
            return ApiResponse(true, response, message, null);
        }

        [NonAction]
        public IActionResult ApiError(object response = null, string message = null, object error = null)
        {
            return ApiResponse(false, response, message, error);
        }

        [NonAction]
        public IActionResult ApiResponse(bool success = true, object response = null, string message = null, object error = null)
        {
            return Json(new { success, error, message, response });
        }
    }
}

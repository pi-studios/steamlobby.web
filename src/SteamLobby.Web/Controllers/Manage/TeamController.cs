﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteamLobby.Web.Controllers.Manage
{
    [Route("/manage/team/")]
    public class TeamController : BaseController
    {
        public TeamController(RequestHelper requestHelper) : base(requestHelper) { }

        [Route("list/{page:int?}")]
        public IActionResult List(int page = 1)
        {
            return RequireAuthentication("SiteAccess.Manage.Team.List", () =>
            {
                page = page - 1;
                return View("List", new ListModel { TotalPages = (int)Math.Ceiling((double)Database.Teams.Count() / 10), CurrentPage = page, EditLink = "/manage/team/{0}/edit/", DeleteLink = "/manage/team/{0}/delete/", CreateLink = "/manage/team/new/", Items = Database.Teams.OrderBy(e => e.Id).Skip(10 * page).Take(10).ToList() });
            });
        }

        [Route("{id:guid}")]
        public IActionResult View(Guid id)
        {
            return RequireAuthentication("SiteAccess.Manage.Team.View", () =>
            {
                ViewBag.Options = new { allowEdit = true };
                return View(Database.Teams.Where(e => e.Id == id).FirstOrDefault());
            });
        }

        [Route("{id:guid}/edit")]
        public IActionResult Edit(Guid id)
        {
            return RequireAuthentication("SiteAccess.Manage.Team.Edit", () =>
            {
                return View(Database.Teams.Where(e => e.Id == id).FirstOrDefault());
            });
        }

        [Route("{id:guid}/delete")]
        public IActionResult Delete(Guid id)
        {
            return RequireAuthentication("SiteAccess.Manage.Team.Delete", () =>
            {
                ViewBag.Options = new { action = "delete" };
                return View("view", Database.Teams.Where(e => e.Id == id).FirstOrDefault());
            });
        }

        [Route("new")]
        public IActionResult New()
        {
            return RequireAuthentication("SiteAccess.Manage.Team.New", () =>
            {
                return View("edit", new Database.Models.Team { Id = Guid.Empty, Game = "csgo", LogoUrl = "/images/defaultteamlogo.jpg" });
            });
        }
    }
}

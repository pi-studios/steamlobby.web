﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteamLobby.Web.Controllers.Manage
{
    [Route("/manage/member/")]
    public class MemberController : BaseController
    {
        public MemberController(RequestHelper requestHelper) : base(requestHelper) { }

        [Route("list/{page:int?}")]
        public IActionResult List(int page = 1, [FromQuery]string q = null)
        {
            return RequireAuthentication("SiteAccess.Manage.Member.List", () =>
            {
                page = page - 1;
                q = q == null ? null : q.ToLower();
                var query = q == null ? Database.Members : Database.Members.Where(e => e.ProfileId.ToLower().StartsWith(q) || e.Email.ToLower().StartsWith(q) || e.Email.ToLower().EndsWith(q) || e.ProfileName.ToLower().StartsWith(q));

                return View("List", new ListModel { TotalPages = (int)Math.Ceiling((double)query.Count() / 10), CurrentPage = page, EditLink = "/manage/member/{0}/edit/", DeleteLink = "/manage/member/{0}/delete/", Items = query.OrderBy(e => e.Id).Skip(10 * page).Take(10).ToList() });
            });
        }

        [Route("{id:guid}")]
        public IActionResult View(Guid id)
        {
            return RequireAuthentication("SiteAccess.Manage.Member.View", () =>
            {
                ViewBag.Options = new { allowEdit = true };
                return View(Database.Members.Where(e => e.Id == id).FirstOrDefault());
            });
        }

        [Route("{id:guid}/edit")]
        public IActionResult Edit(Guid id)
        {
            return RequireAuthentication("SiteAccess.Manage.Member.Edit", () =>
            {
                return View(Database.Members.Where(e => e.Id == id).FirstOrDefault());
            });
        }

        [Route("{id:guid}/delete")]
        public IActionResult Delete(Guid id)
        {
            return RequireAuthentication("SiteAccess.Manage.Member.Delete", () =>
            {
                ViewBag.Options = new { action = "delete" };
                return View("view", Database.Members.Where(e => e.Id == id).FirstOrDefault());
            });
        }
    }
}

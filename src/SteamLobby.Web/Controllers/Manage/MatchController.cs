﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteamLobby.Web.Controllers.Manage
{
    [Route("/manage/match/")]
    public class MatchController : BaseController
    {
        public MatchController(RequestHelper requestHelper) : base(requestHelper) { }

        [Route("list/{page:int=1}")]
        public IActionResult List(int page)
        {
            return Json(null);
        }

        [Route("{id:guid?}/edit/")]
        public IActionResult Edit(Guid id)
        {
            return Json(null);
        }
    }
}

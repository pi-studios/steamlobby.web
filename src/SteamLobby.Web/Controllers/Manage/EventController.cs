﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteamLobby.Web.Controllers.Manage
{
    [Route("/manage/event/")]
    public class EventController : BaseController
    {
        public EventController(RequestHelper requestHelper) : base(requestHelper) { }

        [Route("list/{page:int?}")]
        public IActionResult List(int page = 1)
        {
            return RequireAuthentication("SiteAccess.Manage.Event.List", () =>
            {
                page = page - 1;
                return View("List", new ListModel { TotalPages = (int)Math.Ceiling((double)Database.Events.Count() / 10), CurrentPage = page, EditLink = "/manage/event/{0}/edit/", DeleteLink = "/manage/event/{0}/delete/", CreateLink = "/manage/event/new/", Items = Database.Events.OrderBy(e => e.ScheduledStart).Skip(10 * page).Take(10).ToList() });
            });
        }

        [Route("{id:guid}")]
        public IActionResult View(Guid id)
        {
            return RequireAuthentication("SiteAccess.Manage.Event.View", () =>
            {
                ViewBag.Options = new { allowEdit = true };
                return View(Database.Teams.Where(e => e.Id == id).FirstOrDefault());
            });
        }

        [Route("{id:guid}/edit")]
        public IActionResult Edit(Guid id)
        {
            return RequireAuthentication("SiteAccess.Manage.Event.Edit", () =>
            {
                return View(Database.Teams.Where(e => e.Id == id).FirstOrDefault());
            });
        }

        [Route("{id:guid}/delete")]
        public IActionResult Delete(Guid id)
        {
            return RequireAuthentication("SiteAccess.Manage.Event.Delete", () =>
            {
                ViewBag.Options = new { action = "delete" };
                return View("view", Database.Teams.Where(e => e.Id == id).FirstOrDefault());
            });
        }

        [Route("new")]
        public IActionResult New()
        {
            return RequireAuthentication("SiteAccess.Manage.Event.New", () =>
            {
                return View("edit", new Database.Models.Event { Id = Guid.Empty });
            });
        }
    }
}

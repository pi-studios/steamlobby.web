﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using System.Security.Cryptography;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Framework.DependencyInjection;

namespace SteamLobby.Web.Controllers.Public
{
    [Route("/member/")]
    public class MemberController : BaseController
    {
        public MemberController(RequestHelper requestHelper) : base(requestHelper) { }

        public IActionResult Index()
        {
            return this.RequireAuthentication(() => RedirectToAction("ViewMember", new { profileId = RequestHelper.Member.ProfileId }) );
        }

        [Route("{profileId}")]
        public IActionResult ViewMember(string profileId)
        {
            var member = Database.Members.Where(e => e.ProfileId.ToLower() == profileId.ToLower()).FirstOrDefault();

            if (member == null)
                return HttpNotFound();

            ViewBag.AllowEdit = RequestHelper.Member != null && member.Id == RequestHelper.Member.Id;
            return View("view", member);
        }
    }
}

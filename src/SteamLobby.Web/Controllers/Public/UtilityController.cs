﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using System.Net;

namespace SteamLobby.Web.Controllers.Public
{
    [Route("/utility/")]
    public class UtilityController : Controller
    {
        [Route("imageproxy")]
        public async Task<IActionResult> ImageProxy([FromQuery]string url)
        {
            var webRequest = WebRequest.CreateHttp(url);
            var webResponse = await webRequest.GetResponseAsync();
            if (webResponse.ContentLength >= 1024 * 1024 * 2)
                return this.HttpBadRequest();
            return this.File(webResponse.GetResponseStream(), webResponse.ContentType);
        }
    }
}

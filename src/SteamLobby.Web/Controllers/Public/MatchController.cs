﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace SteamLobby.Web.Controllers.Public
{
    [Route("/match/")]
    public class MatchController : BaseController
    {
        public MatchController(RequestHelper requestHelper) : base(requestHelper) { }

        [Route("{game=csgo}/{matchId}")]
        public IActionResult Index(string game, string matchId)
        {
            return View();
        }

        [Route("{game=csgo}/list/{page:int=1}")]
        public IActionResult ListAll(string game, int page)
        {
            return Json(game);
        }

        [Route("{game=csgo}/list/upcoming/{page:int=1}")]
        public IActionResult ListUpcoming(string game, int page)
        {
            return View("List");
        }

        [Route("{game=csgo}/list/live/{page:int=1}")]
        public IActionResult ListLive(string game, int page)
        {
            return View("List");
        }

        [Route("{game=csgo}/list/complete/{page:int=1}")]
        public IActionResult ListComplete(string game, int page)
        {
            return View("List");
        }
    }
}

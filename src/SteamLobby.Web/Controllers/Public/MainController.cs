﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace SteamLobby.Web.Controllers.Public
{
    [Route("/")]
    public class MainController : BaseController
    {

        public MainController(RequestHelper requestHelper) : base(requestHelper) { }

        public IActionResult Index()
        {
            return View();
        }

        [Route("manage")]
        public IActionResult ManageIndex()
        {
            return View();
        }
    }
}

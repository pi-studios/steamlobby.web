﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Text;
using System.Reflection;
using SteamLobby.Web.Database.Models;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.Framework.Runtime;
using Microsoft.AspNet.Hosting;
using Newtonsoft.Json.Linq;

namespace SteamLobby.Web.Controllers.Api
{
    [Route("/api/v1/")]
    public class ApiV1Controller : BaseController
    {
        private readonly IHostingEnvironment hostingEnvironment;

        public ApiV1Controller(IHostingEnvironment hostingEnvironment, RequestHelper requestHelper) : base(requestHelper)
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return Json(new { success = true, error = false, message = "Steam Lobby Web API v1." });
        }

        [Route("/js/dynamiclass.js")]
        public IActionResult ConstructJavascriptModel()
        {
            string[] publicModels = new string[]
            {
                "Match",
                "Team",
                "Member"
            };

            Dictionary<string, dynamic> models = new Dictionary<string, dynamic>();
            foreach (string modelName in publicModels)
            {
                Type modelType = Assembly.GetExecutingAssembly().GetType("SteamLobby.Web.Database.Models." + modelName, false, true);
                models.Add(modelName.Split('.').Last(), Activator.CreateInstance(modelType));
            }

            return Content("window.dynamiclass = {}; window.dynamiclass.base = " + JsonConvert.SerializeObject(models) + "; window.dynamiclass.new = function(className){ return jQuery.extend(true, {}, window.dynamiclass.base[className]); }", "application/javascript");
        }

        [Route("search/member")]
        public IActionResult SearchMembers([FromQuery]string q)
        {
            if (q == null || q.Trim() == "")
                return ApiError(error: "PARAMETER_REQUIRED", message: "Parameter 'q' is required and may not be empty.");

            q = q.ToLower();
            return ApiSuccess(response: Database.Members.Where(e => e.ProfileName.ToLower().StartsWith(q) || e.ProfileId.ToLower().StartsWith(q) || e.Email == q).Take(10).Select(e => e.SafeCopy));
        }

        [Route("search/team")]
        public IActionResult SearchTeams([FromQuery]string q)
        {
            if (q == null || q.Trim() == "")
                return ApiError(error: "PARAMETER_REQUIRED", message: "Parameter 'q' is required and may not be empty.");

            q = q.ToLower();
            return ApiSuccess(response: Database.Teams.Where(e => e.Name.ToLower().StartsWith(q) || e.PublicId == q).Take(10).Select(e => e.SafeCopy));
        }

        [Route("signin")]
        public IActionResult Signin()
        {
            return View();
        }

        [HttpPost] [ValidateAntiForgeryToken] [Route("signin")]
        public IActionResult DoSignin([FromForm]string email, [FromForm]string password, [FromForm]string origin, [FromForm]string publicKey)
        {
            bool isLocalOrigin = (origin.StartsWith("http://steamlobby.com") || origin.StartsWith("https://steamlobby.com") || origin.StartsWith("http://preview.steamlobby.com") || origin.StartsWith("https://preview.steamlobby.com") || origin.StartsWith("http://localhost:8561"));

            if(!isLocalOrigin)
                return ApiError(error: "INVALID_PUBLIC_KEY", message: "An invalid API key was used for this signin request.");

            if (email == null || password == null)
                return ApiError(error: "INVALID_ACCOUNT", message: "Please enter an email and password.");

            var member = Database.Members.Where(e => e.Email == email.ToLowerInvariant()).FirstOrDefault();
            if (member == null)
                return ApiError(error: "INVALID_ACCOUNT", message: "You entered an invalid username or password.");

            var passwordHash = Database.PasswordHashes.Where(e => e.MemberId == member.Id && e.Expiration == null).OrderByDescending(e => e.Creation).FirstOrDefault();
            if (passwordHash == null)
                return ApiError(error: "INVALID_ACCOUNT", message: "You entered an invalid username or password.");

            if (passwordHash.CompareTo(password))
            {
                var session = new Session { MemberId = member.Id, KeyIdentifier = Application.RandomNumberGenerator.GenerateHexString(64), AddressIdentifier = RemoteAddress, Expiration = DateTime.UtcNow.AddMonths(1) };

                Database.Add(session);
                Database.SaveChanges();

                if(isLocalOrigin)
                    Response.Cookies.Append("steamlobby_session", session.KeyIdentifier, new Microsoft.AspNet.Http.CookieOptions() { Expires = session.Expiration, Domain = Application.CookieDomain, Path = "/" });

                return ApiSuccess(message: null, response: new { token = session.KeyIdentifier });
            }

            return ApiError(error: "INVALID_ACCOUNT", message: "You entered an invalid email or password.");
        }

        [Route("signup")]
        public IActionResult Signup()
        {
            return View();
        }

        [HttpPost] [Route("signup/session")]
        public async Task<IActionResult> SignupSession([FromForm(Name = "g-recaptcha-response")]string g_recaptcha_response)
        {
            
            string signupSessionKey = Application.RandomNumberGenerator.GenerateHexString(32).ToLower();
            bool isCaptchaValid = false;

            using (var webClient = new WebClient())
            {
                var postData = new NameValueCollection()
                {
                    { "secret", Application.Configuration.Get("recaptcha:secretKey") },
                    { "response", g_recaptcha_response }
                };

                dynamic response = JsonConvert.DeserializeObject(Encoding.Default.GetString(await webClient.UploadValuesTaskAsync("https://www.google.com/recaptcha/api/siteverify", postData)));
                isCaptchaValid = response.success;
            }

            if (isCaptchaValid)
                Application.MemoryCache.AddOrGetExisting("signupsession-" + signupSessionKey, true, DateTime.Now.AddMinutes(15));

            return ApiResponse(success: isCaptchaValid, error: isCaptchaValid ? "INVALID_CAPTCHA" : null, response: signupSessionKey);
        }

        [HttpPost] [ValidateAntiForgeryToken] [Route("signup")]
        public IActionResult DoSignup([FromForm]string signupSessionKey, [FromForm]string email, [FromForm]string password, [FromForm]string country, [FromForm]string profileId, [FromForm]string profileName, [FromForm]string profileBirthday, [FromForm]string profileAboutMe, [FromForm]string acceptTerms, [FromForm]string origin, [FromForm]string publicKey)
        {
            if (!Application.MemoryCache.Contains("signupsession-" + signupSessionKey))
                return ApiError(error: "FAIL_SIGNUPSESSION");

            bool isLocalOrigin = (origin.StartsWith("http://steamlobby.com") || origin.StartsWith("https://steamlobby.com") || origin.StartsWith("http://preview.steamlobby.com") || origin.StartsWith("https://preview.steamlobby.com") || origin.StartsWith("http://localhost:8561"));

            if (!isLocalOrigin)
                return ApiError(error: "INVALID_PUBLIC_KEY", message: "An invalid API key was used for this signin request.");

            bool acceptedTerms = acceptTerms == "true";

            //if (!acceptedTerms)
            //    return Json(new { success = false, error = "FAIL_ACCEPTTERMS" });

            bool failedValidation = false;
            var validationResults = new
            {
                email = new Validator(email).NotEmpty().MinLength(6).Email().InUse((v) => { return Database.Members.Where(e => e.Email == v.ToLower()).Count() > 0; }).Out(ref failedValidation).Result.ToString(),
                password = new Validator(password).NotEmpty().MinLength(6).Out(ref failedValidation).Result.ToString(),
                country = new Validator(country).NotEmpty().Out(ref failedValidation).Result.ToString(),
                profileId = new Validator(profileId).NotEmpty().MinLength(3).MaxLength(16).InUse((v) => { return Database.Members.Where(e => e.ProfileId == v.ToLower()).Count() > 0; }).Out(ref failedValidation).Result.ToString(),
                profileName = new Validator(profileName).NotEmpty().MinLength(3).MaxLength(16).Out(ref failedValidation).Result.ToString(),
                profileBirthday = new Validator(profileBirthday).NotEmpty().Date().Out(ref failedValidation).Result.ToString(),
                profileAboutMe = new Validator(profileAboutMe).IgnoreNull().MaxLength(150).Out(ref failedValidation).Result.ToString()
            };

            if (failedValidation)
                return Json(new { success = false, error = "FAIL_VALIDATION", validationResults = validationResults });

            Member member = new Member();
            member.Email = email.ToLowerInvariant();
            member.ProfileCountry = country;
            member.ProfileId = profileId.ToLowerInvariant();
            member.ProfileName = profileName;
            member.ProfileBirthday = DateTime.Parse(profileBirthday).Date;
            member.ProfileAboutMe = profileAboutMe;

            PasswordHash passwordHash = PasswordHash.Generate(password);
            passwordHash.MemberId = member.Id;

            var session = new Session();
            session.MemberId = member.Id;
            session.KeyIdentifier = Application.RandomNumberGenerator.GenerateHexString(64);
            session.AddressIdentifier = RemoteAddress;
            session.Expiration = DateTime.UtcNow.AddMonths(1);

            Database.Add(member);
            Database.Add(passwordHash);
            Database.Add(session);

            Database.SaveChanges();

            if(isLocalOrigin)
                Response.Cookies.Append("steamlobby_session", session.KeyIdentifier, new Microsoft.AspNet.Http.CookieOptions() { Expires = session.Expiration, Path = "/", Domain = Application.CookieDomain });

            Application.MemoryCache.Remove("signupsession-" + signupSessionKey);
            return Json(new { success = true, session = session });
        }

        [Route("signup/validation/{validatorType}")]
        public IActionResult Validator([FromForm]string signupSessionKey, string validatorType, [FromForm]string value)
        {
            if (!Application.MemoryCache.Contains("signupsession-" + signupSessionKey))
                return Json(new { success = false, error = "FAIL_SIGNUPSESSION", redirect = "/member/signup/" });

            ValidatorResult result = ValidatorResult.UNKNOWN;

            if (value != null)
            {
                switch (validatorType)
                {
                    case "email": result = new Validator(value).NotEmpty().MinLength(6).Email().InUse((v) => { return Database.Members.Where(e => e.Email == v.ToLower()).Count() > 0; }).Result; break;
                    case "password": result = new Validator(value).NotEmpty().MinLength(6).Result; break;
                    case "country": result = new Validator(value).NotEmpty().Result; break;
                    case "profileId": result = new Validator(value).NotEmpty().MinLength(3).MaxLength(16).InUse((v) => { return Database.Members.Where(e => e.ProfileId == v.ToLower()).Count() > 0; }).Result; break;
                    case "profileName": result = new Validator(value).NotEmpty().MinLength(3).MaxLength(16).Result; break;
                    case "profileBirthday": result = new Validator(value).NotEmpty().Date().Result; break;
                    case "profileAboutMe": result = new Validator(value).IgnoreNull().MaxLength(150).Result; break;
                }
            }

            return Json(new { success = result == ValidatorResult.SUCCESS, validationResult = result.ToString() });
        }

        [Route("signout")]
        public async Task<IActionResult> Signout([FromForm]string signupSessionKey, string validatorType, [FromForm]string value)
        {
            if (RequestHelper.SessionKey != null)
            {
                var session = Database.Sessions.Where(e => e.KeyIdentifier == RequestHelper.SessionKey).FirstOrDefault();
                session.Expiration = DateTime.UtcNow;

                await Database.Update(session).Context.SaveChangesAsync();

                Response.Cookies.Delete("sessionKey");

                return ApiSuccess();
            }

            return ApiError();
        }

        [HttpPost] [Route("upload/image/{token?}")]
        public IActionResult UploadImage(string token, [FromForm]string url)
        {
            if (url != null)
            {
                try
                {
                    HttpWebRequest request = HttpWebRequest.CreateHttp(url);
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        if (response.ContentLength >= Application.MaxImageUploadSize)
                            return ApiError(error: "FAIL_MAXSIZE", message: "This image exceeds the max upload size.");

                        string imageUrl = string.Format("/public/images/{0}.png", Application.RandomNumberGenerator.GenerateHexString(32));
                        int imageWidth = 0;
                        int imageHeight = 0;

                        using (var image = Image.FromStream(response.GetResponseStream()))
                        {
                            imageWidth = image.Width;
                            imageHeight = image.Height;
                            image.Save(hostingEnvironment.WebRootPath + imageUrl, ImageFormat.Png);
                        }
                    
                        return ApiSuccess(response: new { imageUrl, imageWidth, imageHeight });
                    }
                }
                catch { return ApiError(error: "FAIL"); }
            }
            else
            {

                if (Request.ContentLength >= Application.MaxImageUploadSize)
                    return ApiError(error: "FAIL_MAXSIZE", message: "This image exceeds the max upload size.");

                string imageUrl = string.Format("/public/images/{0}.png", Application.RandomNumberGenerator.GenerateHexString(32));
                int imageWidth = 0;
                int imageHeight = 0;

                try
                {
                    using (var image = Image.FromStream(Request.Body))
                    {
                        imageWidth = image.Width;
                        imageHeight = image.Height;
                        image.Save(hostingEnvironment.WebRootPath + imageUrl, ImageFormat.Png);
                    }

                    return ApiSuccess(response: new { imageUrl, imageWidth, imageHeight });
                }
                catch { return ApiError(error: "FAIL"); }
            }
        }

        [HttpPost]
        [Route("member/{id:guid?}")]
        public async Task<IActionResult> PostMember(Guid? id, [FromBody]JObject rawMember)
        {
#warning Needs authorization check
            var member = rawMember.ToObject<Member>();
            if (id.HasValue && id != Guid.Empty) // Update existing
            {
                var dbMember = Database.Members.Where(e => e.Id == id.Value).FirstOrDefault();
                member.CopyTo(dbMember);

                await Database.Update(dbMember).Context.SaveChangesAsync();

                return ApiSuccess(response: dbMember);
            }
            else // Create new
            {
                member.Id = Guid.NewGuid();

                if (Database.Members.Where(e => e.ProfileId.ToLower() == member.ProfileId.ToLower()).Count() > 0)
                    return ApiError(error: "FAIL_PROFILEID_INUSE");
                if (Database.Members.Where(e => e.Email.ToLower() == member.Email.ToLower()).Count() > 0)
                    return ApiError(error: "FAIL_EMAIL_INUSE");

                await Database.Members.Add(member).Context.SaveChangesAsync();
                return ApiSuccess(response: member);
            }
        }

        [HttpGet]
        [Route("member/{id:guid}")]
        public IActionResult GetMember(Guid id)
        {
            #warning Needs authorization check
            var member = Database.Members.Where(e => e.Id == id).FirstOrDefault();
            return ApiResponse(success: member != null, response: member);
        }

        [HttpGet]
        [Route("team/{id:guid}")]
        public IActionResult GetTeam(Guid id)
        {
            var team = Database.Teams.Where(e => e.Id == id).FirstOrDefault();
            return ApiResponse(success: team != null, response: team);
        }

        [HttpGet]
        [Route("team/list/{page:int=1}")]
        public IActionResult GetTeamList(int page)
        {
            var totalTeams = Database.Teams.Count();
            var teams = Database.Teams.OrderBy(e => e.Id).Skip(10 * (page - 1)).Take(10).Select(e => e.Id).ToArray();
            return ApiSuccess(response: new { totalTeams, teams });
        }

        [HttpPost]
        [Route("team/{id:guid?}")]
        public async Task<IActionResult> PostTeam(Guid? id, [FromBody]JObject rawTeam)
        {
            var team = rawTeam.ToObject<Team>();
#warning Needs authorization check
            if (id.HasValue && id != Guid.Empty) // Update existing
            {
                var dbTeam = Database.Teams.Where(e => e.Id == id.Value).FirstOrDefault();
                team.CopyTo(dbTeam);

                await Database.Update(dbTeam).Context.SaveChangesAsync();

                return ApiSuccess(response: dbTeam);
            }
            else // Create new
            {
                team.Id = Guid.NewGuid();
                await Database.Add(team).Context.SaveChangesAsync();

                return ApiSuccess(response: team);
            }
        }

        [HttpPost]
        [Route("team/{id:guid}/members")]
        public async Task<IActionResult> PostTeamMembers(Guid id, [FromBody]Guid[] memberIds)
        {
            #warning Needs authorization check
            var team = Database.Teams.Where(e => e.Id == id).FirstOrDefault();

            if (team == null)
                return ApiError(error: "FAIL_INVALID_TEAM");

            int addedMembers = 0;
            int removedMembers = 0;

            foreach (Member member in team.Members)
                if (!memberIds.Contains(member.Id))
                {
                    member.TeamId = Guid.Empty;
                    Database.Update(member);
                    removedMembers++;
                }
            foreach (Guid memberId in memberIds)
                if (team.Members.Where(e => e.Id == memberId).Count() == 0)
                {
                    var member = Database.Members.Where(e => e.Id == memberId).FirstOrDefault();
                    if(member != null)
                    {
                        member.TeamId = id;
                        Database.Update(member);
                        addedMembers++;
                    }
                }

            await Database.SaveChangesAsync();

            return ApiSuccess(response: new { addedMembers, removedMembers });
        }

        [HttpGet]
        [Route("match/{id:guid}")]
        public IActionResult GetMatch(Guid id)
        {
            var match = Database.Matches.Where(e => e.Id == id).FirstOrDefault();
            return ApiResponse(success: match != null, response: match);
        }

        [HttpPost]
        [Route("match/{id:guid?}")]
        public async Task<IActionResult> PostMatch(Guid? id, [FromBody]JObject rawMatch)
        {
#warning Needs authorization check
            var match = rawMatch.ToObject<Match>();
            if (id.HasValue && id != Guid.Empty) // Update existing
            {
                var dbMatch = Database.Matches.Where(e => e.Id == id.Value).FirstOrDefault();
                match.CopyTo(dbMatch);
                await Database.Update(match).Context.SaveChangesAsync();

                return ApiSuccess();
            }
            else // Create new
            {
                match.Id = new Guid();
                await Database.Add(match).Context.SaveChangesAsync();

                return ApiSuccess();
            }
        }
    }
}

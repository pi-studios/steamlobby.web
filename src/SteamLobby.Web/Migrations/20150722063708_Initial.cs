using System.Collections.Generic;
using Microsoft.Data.Entity.Relational.Migrations;
using Microsoft.Data.Entity.Relational.Migrations.Builders;
using Microsoft.Data.Entity.Relational.Migrations.Operations;

namespace SteamLobby.Web.Migrations
{
    public partial class Initial : Migration
    {
        public override void Up(MigrationBuilder migration)
        {
            migration.CreateSequence(
                name: "DefaultSequence",
                type: "bigint",
                startWith: 1L,
                incrementBy: 10);
            migration.CreateTable(
                name: "Event",
                columns: table => new
                {
                    Id = table.Column(type: "uniqueidentifier", nullable: false),
                    BannerUrl = table.Column(type: "nvarchar(max)", nullable: true),
                    LogoSmallUrl = table.Column(type: "nvarchar(max)", nullable: true),
                    LogoUrl = table.Column(type: "nvarchar(max)", nullable: true),
                    Name = table.Column(type: "nvarchar(max)", nullable: true),
                    RawData = table.Column(type: "nvarchar(max)", nullable: true),
                    ScheduledEnd = table.Column(type: "datetime2", nullable: true),
                    ScheduledStart = table.Column(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event", x => x.Id);
                });
            migration.CreateTable(
                name: "Match",
                columns: table => new
                {
                    Id = table.Column(type: "uniqueidentifier", nullable: false),
                    AwayTeamId = table.Column(type: "uniqueidentifier", nullable: false),
                    Completed = table.Column(type: "datetime2", nullable: true),
                    EventId = table.Column(type: "uniqueidentifier", nullable: false),
                    HomeTeamId = table.Column(type: "uniqueidentifier", nullable: false),
                    HomeTeamWin = table.Column(type: "bit", nullable: true),
                    RawData = table.Column(type: "nvarchar(max)", nullable: true),
                    RawScoreData = table.Column(type: "nvarchar(max)", nullable: true),
                    ScheduledStart = table.Column(type: "datetime2", nullable: true),
                    State = table.Column(type: "tinyint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Match", x => x.Id);
                });
            migration.CreateTable(
                name: "Member",
                columns: table => new
                {
                    Id = table.Column(type: "uniqueidentifier", nullable: false),
                    Email = table.Column(type: "nvarchar(max)", nullable: true),
                    HeaderUrl = table.Column(type: "nvarchar(max)", nullable: true),
                    Permissions = table.Column(type: "nvarchar(max)", nullable: true),
                    ProfileAboutMe = table.Column(type: "nvarchar(max)", nullable: true),
                    ProfileAvatarUrl = table.Column(type: "nvarchar(max)", nullable: true),
                    ProfileBirthday = table.Column(type: "datetime2", nullable: false),
                    ProfileCountry = table.Column(type: "nvarchar(max)", nullable: true),
                    ProfileId = table.Column(type: "nvarchar(max)", nullable: true),
                    ProfileName = table.Column(type: "nvarchar(max)", nullable: true),
                    TeamId = table.Column(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Member", x => x.Id);
                });
            migration.CreateTable(
                name: "PasswordHash",
                columns: table => new
                {
                    Id = table.Column(type: "uniqueidentifier", nullable: false),
                    Creation = table.Column(type: "datetime2", nullable: false),
                    Expiration = table.Column(type: "datetime2", nullable: true),
                    Hash = table.Column(type: "varbinary(max)", nullable: true),
                    Iterations = table.Column(type: "int", nullable: false),
                    Length = table.Column(type: "int", nullable: false),
                    MemberId = table.Column(type: "uniqueidentifier", nullable: false),
                    Salt = table.Column(type: "varbinary(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PasswordHash", x => x.Id);
                });
            migration.CreateTable(
                name: "Session",
                columns: table => new
                {
                    Id = table.Column(type: "uniqueidentifier", nullable: false),
                    AddressIdentifier = table.Column(type: "nvarchar(max)", nullable: true),
                    ApiIdentifier = table.Column(type: "nvarchar(max)", nullable: true),
                    ClientIdentifier = table.Column(type: "nvarchar(max)", nullable: true),
                    Expiration = table.Column(type: "datetime2", nullable: false),
                    KeyIdentifier = table.Column(type: "nvarchar(max)", nullable: true),
                    MemberId = table.Column(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Session", x => x.Id);
                });
            migration.CreateTable(
                name: "Team",
                columns: table => new
                {
                    Id = table.Column(type: "uniqueidentifier", nullable: false),
                    Division = table.Column(type: "nvarchar(max)", nullable: true),
                    Game = table.Column(type: "nvarchar(max)", nullable: true),
                    HeaderUrl = table.Column(type: "nvarchar(max)", nullable: true),
                    LogoUrl = table.Column(type: "nvarchar(max)", nullable: true),
                    Name = table.Column(type: "nvarchar(max)", nullable: true),
                    PublicId = table.Column(type: "nvarchar(max)", nullable: true),
                    ShortName = table.Column(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Team", x => x.Id);
                });
        }
        
        public override void Down(MigrationBuilder migration)
        {
            migration.DropSequence("DefaultSequence");
            migration.DropTable("Event");
            migration.DropTable("Match");
            migration.DropTable("Member");
            migration.DropTable("PasswordHash");
            migration.DropTable("Session");
            migration.DropTable("Team");
        }
    }
}

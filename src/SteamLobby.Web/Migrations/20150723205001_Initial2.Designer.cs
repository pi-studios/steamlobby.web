using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Relational.Migrations.Infrastructure;
using SteamLobby.Web.Database;

namespace SteamLobby.Web.Migrations
{
    [ContextType(typeof(DatabaseContext))]
    partial class Initial2
    {
        public override string Id
        {
            get { return "20150723205001_Initial2"; }
        }
        
        public override string ProductVersion
        {
            get { return "7.0.0-beta5-13549"; }
        }
        
        public override void BuildTargetModel(ModelBuilder builder)
        {
            builder
                .Annotation("SqlServer:DefaultSequenceName", "DefaultSequence")
                .Annotation("SqlServer:Sequence:.DefaultSequence", "'DefaultSequence', '', '1', '10', '', '', 'Int64', 'False'")
                .Annotation("SqlServer:ValueGeneration", "Sequence");
            
            builder.Entity("SteamLobby.Web.Database.Models.Event", b =>
                {
                    b.Property<Guid>("Id")
                        .GenerateValueOnAdd();
                    
                    b.Property<string>("BannerUrl");
                    
                    b.Property<string>("LogoSmallUrl");
                    
                    b.Property<string>("LogoUrl");
                    
                    b.Property<string>("Name");
                    
                    b.Property<string>("RawData");
                    
                    b.Property<DateTime?>("ScheduledEnd");
                    
                    b.Property<DateTime?>("ScheduledStart");
                    
                    b.Property<string>("TeamList");
                    
                    b.Key("Id");
                });
            
            builder.Entity("SteamLobby.Web.Database.Models.Match", b =>
                {
                    b.Property<Guid>("Id")
                        .GenerateValueOnAdd();
                    
                    b.Property<Guid>("AwayTeamId");
                    
                    b.Property<DateTime?>("Completed");
                    
                    b.Property<Guid>("EventId");
                    
                    b.Property<Guid>("HomeTeamId");
                    
                    b.Property<bool?>("HomeTeamWin");
                    
                    b.Property<string>("RawData");
                    
                    b.Property<string>("RawScoreData");
                    
                    b.Property<DateTime?>("ScheduledStart");
                    
                    b.Property<byte?>("State");
                    
                    b.Key("Id");
                });
            
            builder.Entity("SteamLobby.Web.Database.Models.Member", b =>
                {
                    b.Property<Guid>("Id")
                        .GenerateValueOnAdd();
                    
                    b.Property<string>("Email");
                    
                    b.Property<string>("HeaderUrl");
                    
                    b.Property<string>("Permissions");
                    
                    b.Property<string>("ProfileAboutMe");
                    
                    b.Property<string>("ProfileAvatarUrl");
                    
                    b.Property<DateTime>("ProfileBirthday");
                    
                    b.Property<string>("ProfileCountry");
                    
                    b.Property<string>("ProfileId");
                    
                    b.Property<string>("ProfileName");
                    
                    b.Property<Guid>("TeamId");
                    
                    b.Key("Id");
                });
            
            builder.Entity("SteamLobby.Web.Database.Models.PasswordHash", b =>
                {
                    b.Property<Guid>("Id")
                        .GenerateValueOnAdd();
                    
                    b.Property<DateTime>("Creation");
                    
                    b.Property<DateTime?>("Expiration");
                    
                    b.Property<byte[]>("Hash");
                    
                    b.Property<int>("Iterations");
                    
                    b.Property<int>("Length");
                    
                    b.Property<Guid>("MemberId");
                    
                    b.Property<byte[]>("Salt");
                    
                    b.Key("Id");
                });
            
            builder.Entity("SteamLobby.Web.Database.Models.Session", b =>
                {
                    b.Property<Guid>("Id")
                        .GenerateValueOnAdd();
                    
                    b.Property<string>("AddressIdentifier");
                    
                    b.Property<string>("ApiIdentifier");
                    
                    b.Property<string>("ClientIdentifier");
                    
                    b.Property<DateTime>("Expiration");
                    
                    b.Property<string>("KeyIdentifier");
                    
                    b.Property<Guid>("MemberId");
                    
                    b.Key("Id");
                });
            
            builder.Entity("SteamLobby.Web.Database.Models.Team", b =>
                {
                    b.Property<Guid>("Id")
                        .GenerateValueOnAdd();
                    
                    b.Property<string>("Division");
                    
                    b.Property<string>("Game");
                    
                    b.Property<string>("HeaderUrl");
                    
                    b.Property<string>("LogoUrl");
                    
                    b.Property<string>("Name");
                    
                    b.Property<string>("PublicId");
                    
                    b.Property<string>("ShortName");
                    
                    b.Key("Id");
                });
        }
    }
}

using System.Collections.Generic;
using Microsoft.Data.Entity.Relational.Migrations;
using Microsoft.Data.Entity.Relational.Migrations.Builders;
using Microsoft.Data.Entity.Relational.Migrations.Operations;

namespace SteamLobby.Web.Migrations
{
    public partial class Initial3 : Migration
    {
        public override void Up(MigrationBuilder migration)
        {
            migration.DropColumn(name: "LogoSmallUrl", table: "Event");
        }
        
        public override void Down(MigrationBuilder migration)
        {
            migration.AddColumn(
                name: "LogoSmallUrl",
                table: "Event",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

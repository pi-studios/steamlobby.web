﻿var timeouts = [];
var previousValues = [];

$(function()
{
    $('*[data-validation=True]').keyup(function()
    {
        runValidation($(this));
    }).change(function()
    {
        runValidation($(this));
    })
})

window.runValidation = function(selector, delay)
{
    var value = selector.val();
    var id = selector.attr("id");

    if (delay || previousValues[id] != value)
    {
        previousValues[id] = value;

        if (timeouts[id])
            clearTimeout(timeouts[id]);

        timeouts[id] = setTimeout(function()
        {
            $.post("/api/v1/signup/validation/" + id, { signupSessionKey: window.signupSessionKey, value: value }).done(function(response)
            {
                if (response.error && response.redirect)
                    window.location = response.redirect;

                handleValidationResponse(selector, response)
            }).error(function(response)
            {
                if (response.status == 429)
                    runValidation(selector, parseInt(response.getResponseHeader("Retry-After")) * 1000 + 100)
            });
        }, delay || 500)
    }
};

window.handleValidationResponse = function(selector, response)
{
    var parentSelector = selector.parent();

    var id = selector.attr("id");
    //var labelSelector = $("label[for=" + id + "]");

    if (response.validationResult != "SUCCESS")
    {
        parentSelector.addClass("is-invalid");
        $('.mdl-textfield__error', parentSelector).text(window.steamlobby.language[window.steamlobby.selectedLanguage].error_messages[id + "_validation_" + response.validationResult]);
    }
    else
    {
        parentSelector.removeClass("is-invalid");
    }

    console.debug("[ValidationResponse]", id, "=>", response.validationResult);
};
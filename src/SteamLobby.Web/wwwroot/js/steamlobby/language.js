﻿if (!window.steamlobby)
    window.steamlobby = {};

window.steamlobby.language = {
    en_us: {
        error_messages: {
            email_validation_FAIL_NOTEMPTY: "An email is required.",
            email_validation_FAIL_MINLENGTH: "This email is too short.",
            email_validation_FAIL_EMAIL: "Please enter a valid email.",
            email_validation_FAIL_INUSE: "This email is already in use.",
            password_validation_FAIL_NOTEMPTY: "A password is required.",
            password_validation_FAIL_MINLENGTH: "Your password must be at least 6 characters.",
            country_validation_FAIL_SETLENGTH: "Please select a valid country.",
            country_validation_FAIL_NOTEMPTY: "Please select a valid country.",
            profileId_validation_FAIL_NOTEMPTY: "A profile ID is required.",
            profileId_validation_FAIL_MINLENGTH: "Your profile id must be at least 3 characters.",
            profileId_validation_FAIL_MAXLENGTH: "Your profile id must be at most 16 characters.",
            profileId_validation_FAIL_INUSE: "This profile id is already in use.",
            profileName_validation_FAIL_NOTEMPTY: "A display name is required.",
            profileName_validation_FAIL_MINLENGTH: "Your display name must be at least 3 characters.",
            profileName_validation_FAIL_MAXLENGTH: "Your display name must be at most 16 characters.",
            profileBirthday_validation_FAIL_DATE: "Please select a valid date.",
            profileBirthday_validation_FAIL_NOTEMPTY: "Please select a valid date."
        }
    }
}

window.steamlobby.selectedLanguage = "en_us";
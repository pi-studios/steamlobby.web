﻿$(function()
{
    $('.preload').fadeOut(400);

    $('.body-overlay').click(function()
    {
        $('.sidenav').removeClass('open');
        $('.body-overlay').removeClass('visible');
    });

    $('.icon-menu').click(function()
    {
        $('.sidenav').addClass('open');
        $('.body-overlay').addClass('visible');
    });

    $('.expandable .header').click(function()
    {
        var selector = $(this).parent();
        if (selector.hasClass("expanded"))
            selector.removeClass("expanded");
        else
            selector.addClass("expanded");
    });

    $('.dialog').dialog();
})

Array.prototype.remove = function()
{
    var what, a = arguments, L = a.length, ax;
    while (L && this.length)
    {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1)
        {
            this.splice(ax, 1);
        }
    }
    return this;
};

jQuery.fn.extend(
{
    drop: function(bottom, duration, complete)
    {
        return this.css({ position: 'fixed' }).animate({ bottom: bottom || -this.height() }, duration || 300, "easeInQuad", complete || function() { this.remove(); });
    },

    dialog: function(option)
    {
        option = option || 'setup';

        if (option == 'show')
            this.addClass("visible");

        if (option == 'hide')
            this.removeClass("visible");

        if (option == 'setup')
        {
            this.click(function()
            {
                $(this).removeClass("visible");
            });
        }

        return this;
    }
});

window.doSignout = function()
{
    $.get("/api/v1/signout/", function(response)
    {
        if (response.success)
        {
            $('#accountFAB').drop();
            //window.location.reload();
        }
    })
};

window.showSignin = function()
{
    $('#signinModal').remove();
    var dialog = $('<div id="signinModal" class="dialog"><iframe src="/api/v1/signin/" seamless allowtransparency="true" scrolling="auto" style="border: none; height: 350px;" class="mdl-cell--4-col"></iframe></div>');
    var iframe = $('iframe', dialog)[0];

    iframe.onload = function()
    {
        iframe.contentWindow.postMessage({ type: "auth", publicKey: null }, "*");
    };

    dialog.appendTo("body").dialog().dialog('show');
};

window.closeSignin = function()
{
    $('#signinModal').dialog('hide');
};

window.showSignup = function()
{
    $('#signupModal').remove();
    var dialog = $('<div id="signupModal" class="dialog"><iframe src="/api/v1/signup/" seamless allowtransparency="true" scrolling="auto" style="border: none; height: 500px; width: 100%; max-width: 500px"></iframe></div>');
    var iframe = $('iframe', dialog)[0];

    iframe.onload = function()
    {
        iframe.contentWindow.postMessage({ type: "auth", publicKey: null }, "*");
    };

    dialog.appendTo("body").dialog().dialog('show');
};

window.closeSignup = function()
{
    $('#signupModal').dialog('hide');
};

window.addEventListener("message", receiveMessage, false);

function receiveMessage(event)
{
    console.log(event);

    if (event.data && event.data.type == "showSignin")
    {
        window.closeSignup();
        window.showSignin();
    }

    if (event.data && event.data.type == "showSignup")
    {
        window.closeSignin();
        window.showSignup();
    }

    if (event.data && event.data.type == "hideSignin")
    {
        window.closeSignin();
    }

    if (event.data && event.data.type == "hideSignup")
    {
        window.closeSignup();
    }

    if (event.data && event.data.type == "signinResponse")
        if (event.data.response.success)
        {
            window.closeSignin();
            $('#signinFAB').drop();
            window.location.reload();
        }

    if (event.data && event.data.type == "signupResponse")
        if (event.data.response.success)
        {
            window.closeSignup();
            $('#signinFAB').drop();
            window.location.reload();
        }
}
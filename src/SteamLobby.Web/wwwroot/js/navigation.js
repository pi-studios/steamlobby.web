﻿if (typeof String.prototype.startsWith != 'function')
{
    String.prototype.startsWith = function(str)
    {
        return this.slice(0, str.length) == str;
    };
}
if (typeof String.prototype.endsWith != 'function')
{
    String.prototype.endsWith = function(str)
    {
        return this.slice(-str.length) == str;
    };
}

$(function()
{
    $('a').click(function(event)
    {
        event.preventDefault();
        window.navigation.navigate($(this).prop("href"));
    });
});

(function()
{
    if (window.history)
    {
        window.history.replaceState($('#mainContent').html(), null, window.location.pathname);
        window.onpopstate = function(event)
        {
            $('#mainContent').html(event.state);
        }
    }

    window.navigation = {
        navigate: function(url)
        {
            if (url.startsWith(window.location.origin) && window.history)
                this.navigateAsync(url);
            else
                window.location = url;
        },
        navigateAsync: function(url)
        {
            window.material.scope.pageLoadProgress.mode = "indeterminate"
            window.material.scope.pageLoadProgress.visibility = "visible";
            window.material.updateScope();

            $.ajax({
                xhr: function()
                {
                    var xhr = new window.XMLHttpRequest();

                    xhr.addEventListener("progress", function(evt)
                    {
                        if (evt.lengthComputable)
                        {
                            window.material.scope.pageLoadProgress.mode = "determinate"
                            window.material.scope.pageLoadProgress.value = (evt.loaded / evt.total) * 100;
                            window.material.updateScope();
                        }
                    }, false);

                    return xhr;
                },
                type: 'GET',
                url: url,
                data: "requestedResponse=content",
                processData: false,
                success: function(response)
                {
                    if (typeof response === "object")
                        response = "<pre>" + JSON.stringify(response, null, '\t') + "</pre>";

                    $('#mainContent').html(response);
                    window.history.pushState(response, "", url);
                    window.material.scope.pageLoadProgress.visibility = "hidden";
                    window.material.updateScope();
                }
            });
        },
        forward: function()
        {

        },
        back: function()
        {

        }
    }
})();
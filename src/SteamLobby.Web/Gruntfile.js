/// <binding AfterBuild='bowerInstall' Clean='bowerCleanInstall' />
/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkID=513275&clcid=0x409
*/
module.exports = function (grunt) {
    grunt.initConfig({
        bower: {
            install: {
                options: {
                    targetDir: "wwwroot/lib",
                    layout: "byComponent",
                    cleanTargetDir: false
                }
            },
            cleanInstall: {
                options: {
                        targetDir: "wwwroot/lib",
                        layout: "byComponent",
                        cleanTargetDir: true
                }
            }
        },
    });

    grunt.registerTask("bowerInstall", ["bower:install"]);
    grunt.registerTask("bowerCleanInstall", ["bower:cleanInstall"]);

    grunt.loadNpmTasks("grunt-bower-task");
};
﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Diagnostics;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.AspNet.Routing;
using Microsoft.Framework.DependencyInjection;
using System;
using System.Security.Cryptography;
using System.Runtime.Caching;
using System.Collections.Generic;
using SteamLobby.Web.Middleware;
using Microsoft.Framework.Configuration;
using Microsoft.Framework.Runtime;
using Microsoft.Framework.Logging;
using Microsoft.AspNet.Diagnostics.Entity;

namespace SteamLobby.Web
{
    public static class Application
    {
        public static readonly bool _NO_AUTH = false;

        public static IConfiguration Configuration { get; private set; }
        public static IHostingEnvironment HostingEnvironment { get; private set; }
        public static IApplicationEnvironment ApplicationEnvironment { get; private set; }
        public static IServiceCollection Services { get; private set; }
        public static IApplicationBuilder Builder { get; private set; }
        public static MemoryCache MemoryCache { get; } = new MemoryCache("applicationMemoryCache");
        public static RandomNumberGenerator RandomNumberGenerator { get; } = RandomNumberGenerator.Create();
        public static string CookieDomain
        {
            get
            {
                string domain = Configuration.Get("application:domains:cookie");
                return domain == "localhost" ? null : domain;
            }
        }
        public static string HttpDomain
        {
            get
            {
                return Configuration.Get("application:domains:http");
            }
        }
        public static string HttpsDomain
        {
            get
            {
                return Configuration.Get("application:domains:https");
            }
        }
        public static long MaxImageUploadSize
        {
            get
            {
                return long.Parse(Configuration.Get("application:maxImageUploadSize"));
            }
        }

        public static class Constants
        {
            public static readonly string[] Countries = new string[]
            {
                "United States of America",
                "Afghanistan",
                "ALA Aland Islands",
                "Albania",
                "Algeria",
                "American Samoa",
                "Andorra",
                "Angola",
                "Anguilla",
                "Antarctica",
                "Antigua and Barbuda",
                "Argentina",
                "Armenia",
                "Aruba",
                "Australia",
                "Austria",
                "Azerbaijan",
                "Bahamas",
                "Bahrain",
                "Bangladesh",
                "Barbados",
                "Belarus",
                "Belgium",
                "Belize",
                "Benin",
                "Bermuda",
                "Bhutan",
                "Bolivia",
                "Bosnia and Herzegovina",
                "Botswana",
                "Bouvet Island",
                "Brazil",
                "British Virgin Islands",
                "British Indian Ocean Territory",
                "Brunei Darussalam",
                "Bulgaria",
                "Burkina Faso",
                "Burundi",
                "Cambodia",
                "Cameroon",
                "Canada",
                "Cape Verde",
                "Cayman Islands",
                "Central African Republic",
                "Chad",
                "Chile",
                "China",
                "Hong Kong, Special Administrative Region of China",
                "Macao, Special Administrative Region of China",
                "Christmas Island",
                "Cocos (Keeling) Islands",
                "Colombia",
                "Comoros",
                "Congo (Brazzaville)",
                "Congo, Democratic Republic of the",
                "Cook Islands",
                "Costa Rica",
                "Côte d'Ivoire",
                "Croatia",
                "Cuba",
                "Cyprus",
                "Czech Republic",
                "Denmark",
                "Djibouti",
                "Dominica",
                "Dominican Republic",
                "Ecuador",
                "Egypt",
                "El Salvador",
                "Equatorial Guinea",
                "Eritrea",
                "Estonia",
                "Ethiopia",
                "Falkland Islands (Malvinas)",
                "Faroe Islands",
                "Fiji",
                "Finland",
                "France",
                "French Guiana",
                "French Polynesia",
                "French Southern Territories",
                "Gabon",
                "Gambia",
                "Georgia",
                "Germany",
                "Ghana",
                "Gibraltar",
                "Greece",
                "Greenland",
                "Grenada",
                "Guadeloupe",
                "Guam",
                "Guatemala",
                "Guernsey",
                "Guinea",
                "Guinea-Bissau",
                "Guyana",
                "Haiti",
                "Heard Island and Mcdonald Islands",
                "Holy See (Vatican City State)",
                "Honduras",
                "Hungary",
                "Iceland",
                "India",
                "Indonesia",
                "Iran, Islamic Republic of",
                "Iraq",
                "Ireland",
                "Isle of Man",
                "Israel",
                "Italy",
                "Jamaica",
                "Japan",
                "Jersey",
                "Jordan",
                "Kazakhstan",
                "Kenya",
                "Kiribati",
                "Korea, Democratic People's Republic of",
                "Korea, Republic of",
                "Kuwait",
                "Kyrgyzstan",
                "Lao PDR",
                "Latvia",
                "Lebanon",
                "Lesotho",
                "Liberia",
                "Libya",
                "Liechtenstein",
                "Lithuania",
                "Luxembourg",
                "Macedonia, Republic of",
                "Madagascar",
                "Malawi",
                "Malaysia",
                "Maldives",
                "Mali",
                "Malta",
                "Marshall Islands",
                "Martinique",
                "Mauritania",
                "Mauritius",
                "Mayotte",
                "Mexico",
                "Micronesia, Federated States of",
                "Moldova",
                "Monaco",
                "Mongolia",
                "Montenegro",
                "Montserrat",
                "Morocco",
                "Mozambique",
                "Myanmar",
                "Namibia",
                "Nauru",
                "Nepal",
                "Netherlands",
                "Netherlands Antilles",
                "New Caledonia",
                "New Zealand",
                "Nicaragua",
                "Niger",
                "Nigeria",
                "Niue",
                "Norfolk Island",
                "Northern Mariana Islands",
                "Norway",
                "Oman",
                "Pakistan",
                "Palau",
                "Palestinian Territory, Occupied",
                "Panama",
                "Papua New Guinea",
                "Paraguay",
                "Peru",
                "Philippines",
                "Pitcairn",
                "Poland",
                "Portugal",
                "Puerto Rico",
                "Qatar",
                "Réunion",
                "Romania",
                "Russian Federation",
                "Rwanda",
                "Saint-Barthélemy",
                "Saint Helena",
                "Saint Kitts and Nevis",
                "Saint Lucia",
                "Saint-Martin (French part)",
                "Saint Pierre and Miquelon",
                "Saint Vincent and Grenadines",
                "Samoa",
                "San Marino",
                "Sao Tome and Principe",
                "Saudi Arabia",
                "Senegal",
                "Serbia",
                "Seychelles",
                "Sierra Leone",
                "Singapore",
                "Slovakia",
                "Slovenia",
                "Solomon Islands",
                "Somalia",
                "South Africa",
                "South Georgia and the South Sandwich Islands",
                "South Sudan",
                "Spain",
                "Sri Lanka",
                "Sudan",
                "Suriname *",
                "Svalbard and Jan Mayen Islands",
                "Swaziland",
                "Sweden",
                "Switzerland",
                "Syrian Arab Republic (Syria)",
                "Taiwan, Republic of China",
                "Tajikistan",
                "Tanzania *, United Republic of",
                "Thailand",
                "Timor-Leste",
                "Togo",
                "Tokelau",
                "Tonga",
                "Trinidad and Tobago",
                "Tunisia",
                "Turkey",
                "Turkmenistan",
                "Turks and Caicos Islands",
                "Tuvalu",
                "Uganda",
                "Ukraine",
                "United Arab Emirates",
                "United Kingdom",
                "United States Minor Outlying Islands",
                "Uruguay",
                "Uzbekistan",
                "Vanuatu",
                "Venezuela (Bolivarian Republic of)",
                "Viet Nam",
                "Virgin Islands, US",
                "Wallis and Futuna Islands",
                "Western Sahara",
                "Yemen",
                "Zambia",
                "Zimbabwe"
            };
        }

        public class Startup
        {
            public Startup(IHostingEnvironment hostingEnvironment, IApplicationEnvironment applicationEnvironment)
            {
                Application.HostingEnvironment = hostingEnvironment;
                Application.ApplicationEnvironment = applicationEnvironment;

                var configurationBuilder = new ConfigurationBuilder(Application.ApplicationEnvironment.ApplicationBasePath);
                configurationBuilder.AddJsonFile("config.json");
                configurationBuilder.AddEnvironmentVariables();
                configurationBuilder.AddJsonFile($"config.{Application.HostingEnvironment.EnvironmentName}.json", true);
                configurationBuilder.AddUserSecrets();

                if (Application.HostingEnvironment.IsEnvironment("Development"))
                {
                    
                    configurationBuilder.AddApplicationInsightsSettings(developerMode: true);
                }

                Application.Configuration = configurationBuilder.Build();
            }

            public void ConfigureServices(IServiceCollection services)
            {
                Application.Services = services;
                Application.Services.AddMvc();
                Application.Services.AddApplicationInsightsTelemetry(Application.Configuration);
                Application.Services.AddScoped<RequestHelper>();
                Application.Services.AddEntityFramework().AddSqlServer().AddDbContext<Database.DatabaseContext>();
            }

            public void Configure(IApplicationBuilder applicationBuilder, ILoggerFactory loggerFactory)
            {
                Application.Builder = applicationBuilder;

                loggerFactory.MinimumLevel = LogLevel.Information;
                loggerFactory.AddConsole();

                //if (Application.Environment.IsEnvironment("Development"))
                //Application.Builder.UseResponsiveLogger();

                Application.Builder.UseApplicationInsightsExceptionTelemetry();
                Application.Builder.UseApplicationInsightsRequestTelemetry();

                if (Application.HostingEnvironment.IsEnvironment("Development"))
                {
                    Application.Builder.UseBrowserLink();
                    Application.Builder.UseErrorPage(ErrorPageOptions.ShowAll);
                    Application.Builder.UseDatabaseErrorPage(DatabaseErrorPageOptions.ShowAll);
                    Application.Builder.UseRuntimeInfoPage();
                }

                //Application.Builder.UseStaticFiles();
                Application.Builder.UseMvc();
            }
        }
    }
}

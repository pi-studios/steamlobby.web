﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SteamLobby.Web
{
    public class Validator
    {
        private bool IgnoreValidators { get; set; }

        public string Value { get; private set; }
        public ValidatorResult Result { get; private set; } = ValidatorResult.SUCCESS;

        public Validator(string value)
        {
            this.Value = value;
        }

        public Validator Out(ref bool failedValidation)
        {
            if(!failedValidation)
                failedValidation = this.Result != ValidatorResult.SUCCESS;

            return this;
        }

        public Validator IgnoreNull()
        {
            if (this.Value == null)
                this.IgnoreValidators = true;

            return this;
        }

        public Validator NotEmpty()
        {
            if (this.IgnoreValidators)
                return this;

            if (Result == ValidatorResult.SUCCESS)
                if (this.Value == null || this.Value.Trim() == string.Empty)
                    Result = ValidatorResult.FAIL_NOTEMPTY;
            return this;
        }

        public Validator MinLength(int length)
        {
            if (this.IgnoreValidators)
                return this;

            if (Result == ValidatorResult.SUCCESS)
                if (this.Value.Length < length)
                    Result = ValidatorResult.FAIL_MINLENGTH;
            return this;
        }

        public Validator MaxLength(int length)
        {
            if (this.IgnoreValidators)
                return this;

            if (Result == ValidatorResult.SUCCESS)
                if(this.Value.Length > length)
                    Result = ValidatorResult.FAIL_MAXLENGTH;
            return this;
        }

        public Validator SetLength(int length)
        {
            if (this.IgnoreValidators)
                return this;

            if (Result == ValidatorResult.SUCCESS)
                if (this.Value.Length != length)
                    Result = ValidatorResult.FAIL_SETLENGTH;
            return this;
        }

        public Validator Date()
        {
            if (this.IgnoreValidators)
                return this;

            DateTime date;
            if (Result == ValidatorResult.SUCCESS)
                if (!DateTime.TryParse(this.Value, out date))
                    Result = ValidatorResult.FAIL_DATE;
            return this;
        }

        public Validator Regex(Regex regex)
        {
            if (this.IgnoreValidators)
                return this;

            if (Result == ValidatorResult.SUCCESS)
                if (!regex.IsMatch(this.Value))
                    Result = ValidatorResult.FAIL_REGEX;
            return this;
        }

        public Validator Email()
        {
            if (this.IgnoreValidators)
                return this;

            if (Result == ValidatorResult.SUCCESS)
                if (!System.Text.RegularExpressions.Regex.IsMatch(this.Value,
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)))
                    Result = ValidatorResult.FAIL_EMAIL;
            return this;
        }

        public Validator InUse(Func<string, bool> checkInUse)
        {
            if (this.IgnoreValidators)
                return this;

            if (Result == ValidatorResult.SUCCESS)
                if (checkInUse(this.Value))
                    this.Result = ValidatorResult.FAIL_INUSE;

            return this;
        }
    }

    public enum ValidatorResult
    {
        SUCCESS,
        UNKNOWN,
        FAIL_MINLENGTH,
        FAIL_MAXLENGTH,
        FAIL_SETLENGTH,
        FAIL_NOTEMPTY,
        FAIL_EMAIL,
        FAIL_REGEX,
        FAIL_DATE,
        FAIL_INUSE
    }
}

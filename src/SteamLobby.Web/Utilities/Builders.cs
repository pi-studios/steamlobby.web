﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteamLobby.Web.Utilities
{
    public class Builders
    {
        public class QueryBuilder
        {
            public static string Build(Dictionary<string, string> queryData)
            {
                string result = "";
                
                foreach(var query in queryData)
                    result += query.Key + "=" + query.Value + "&";

                return result.Substring(0, result.Length > 1 ? result.Length - 1 : 0);
            }
        }
    }
}

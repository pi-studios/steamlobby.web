﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using SteamLobby.Web.Database.Models;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
using Microsoft.AspNet.Hosting;

namespace SteamLobby.Web
{
    class _Session { public Session Session { get; set; } public Member Member { get; set; } }

    public class RequestHelper
    {
        private _Session session = null;

        public HttpContext Context { get; }
        public Database.DatabaseContext Database { get; }
        public HttpRequest Request { get { return Context.Request; } }
        public HttpResponse Response { get { return Context.Response; } }
        public Guid RequestGuid { get; }
        public string SessionKey
        {
            get
            {
                if (Request.Cookies.ContainsKey("steamlobby_session"))
                    return Request.Cookies["steamlobby_session"];

                return null;
            }
        }

        public Session Session
        {
            get
            {
                if (session == null && this.SessionKey != null)
                {
                    session = Database.Sessions.Where(e => e.KeyIdentifier == this.SessionKey && e.Expiration > DateTime.UtcNow).Join(Database.Members, sessions => sessions.MemberId, members => members.Id, (sessions, members) => new _Session { Session = sessions, Member = members }).FirstOrDefault();

                    if (session != null && session.Session != null)
                    {
                        session.Session.Expiration = DateTime.UtcNow.AddMonths(1);
                        Database.Update(session.Session);
                        Database.SaveChanges();
                    }
                }

                return session != null ? session.Session : null;
            }
        }

        public Member Member
        {
            get
            {
                if (session == null && this.SessionKey != null)
                {
                    session = Database.Sessions.Where(e => e.KeyIdentifier == this.SessionKey && e.Expiration > DateTime.UtcNow).Join(Database.Members, sessions => sessions.MemberId, members => members.Id, (sessions, members) => new _Session { Session = sessions, Member = members }).FirstOrDefault();

                    if (session != null && session.Session != null)
                    {
                        session.Session.Expiration = DateTime.UtcNow.AddMonths(1);
                        Database.Update(session.Session);
                        Database.SaveChanges();
                    }
                }

                return session != null ? session.Member : null;
            }
        }

        public RequestHelper(IHttpContextAccessor contextAccessor, Database.DatabaseContext databaseContext)
        {
            this.Context = contextAccessor.HttpContext;
            this.Database = databaseContext;
            this.RequestGuid = Guid.NewGuid();
        }
    }
}

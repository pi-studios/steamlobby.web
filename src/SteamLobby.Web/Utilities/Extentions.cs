﻿using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Razor;
using Microsoft.AspNet.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Caching;
using System.Net;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using Microsoft.Framework.DependencyInjection;
using System.Diagnostics;
using Microsoft.ApplicationInsights;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.AspNet.Http.Features;

namespace SteamLobby.Web
{
    /// <summary>
    /// Generic class which copies to its target type from a source
    /// type specified in the Copy method. The types are specified
    /// separately to take advantage of type inference on generic
    /// method arguments.
    /// </summary>
    public static class PropertyCopy<TTarget> where TTarget : class, new()
    {
        /// <summary>
        /// Copies all readable properties from the source to a new instance
        /// of TTarget.
        /// </summary>
        public static TTarget CopyFrom<TSource>(TSource source) where TSource : class
        {
            return PropertyCopier<TSource>.Copy(source);
        }

        /// <summary>
        /// Static class to efficiently store the compiled delegate which can
        /// do the copying. We need a bit of work to ensure that exceptions are
        /// appropriately propagated, as the exception is generated at type initialization
        /// time, but we wish it to be thrown as an ArgumentException.
        /// </summary>
        private static class PropertyCopier<TSource> where TSource : class
        {
            private static readonly Func<TSource, TTarget> copier;
            private static readonly Exception initializationException;

            internal static TTarget Copy(TSource source)
            {
                if (initializationException != null)
                {
                    throw initializationException;
                }
                if (source == null)
                {
                    throw new ArgumentNullException("source");
                }
                return copier(source);
            }

            static PropertyCopier()
            {
                try
                {
                    copier = BuildCopier();
                    initializationException = null;
                }
                catch (Exception e)
                {
                    copier = null;
                    initializationException = e;
                }
            }

            private static Func<TSource, TTarget> BuildCopier()
            {
                ParameterExpression sourceParameter = Expression.Parameter(typeof(TSource), "source");
                var bindings = new List<MemberBinding>();
                foreach (PropertyInfo sourceProperty in typeof(TSource).GetProperties())
                {
                    if (!sourceProperty.CanRead)
                    {
                        continue;
                    }
                    PropertyInfo targetProperty = typeof(TTarget).GetProperty(sourceProperty.Name);
                    if (targetProperty == null)
                    {
                        throw new ArgumentException("Property " + sourceProperty.Name + " is not present and accessible in " + typeof(TTarget).FullName);
                    }
                    if (!targetProperty.CanWrite)
                    {
                        throw new ArgumentException("Property " + sourceProperty.Name + " is not writable in " + typeof(TTarget).FullName);
                    }
                    if (!targetProperty.PropertyType.IsAssignableFrom(sourceProperty.PropertyType))
                    {
                        throw new ArgumentException("Property " + sourceProperty.Name + " has an incompatible type in " + typeof(TTarget).FullName);
                    }
                    bindings.Add(Expression.Bind(targetProperty, Expression.Property(sourceParameter, sourceProperty)));
                }
                Expression initializer = Expression.MemberInit(Expression.New(typeof(TTarget)), bindings);
                return Expression.Lambda<Func<TSource, TTarget>>(initializer, sourceParameter).Compile();
            }
        }
    }

    public static class Extentions
    {
        public static void CopyTo(this object source, object destination)
        {
            var sourceType = source.GetType();
            var destinationType = destination.GetType();

            foreach (var destinationProperty in destinationType.GetProperties())
            {
                var sourceProperty = sourceType.GetProperty(destinationProperty.Name);

                if(sourceProperty != null && sourceProperty.GetGetMethod() != null && destinationProperty.GetSetMethod() != null && destinationProperty.GetCustomAttribute<IgnoreCopyAttribute>() == null && sourceProperty.GetCustomAttribute<IgnoreCopyAttribute>() == null)
                    destinationProperty.SetValue(destination, sourceProperty.GetValue(source));
            }
        }

        public static HtmlString RenderSection(this RazorPage webPage, string name, Func<dynamic, HtmlString> defaultContents)
        {
            if (webPage.IsSectionDefined(name))
                return webPage.RenderSection(name);

            return defaultContents(null);
        }

        public static byte[] GenerateBytes(this RandomNumberGenerator randomNumberGenerator, int length)
        {
            byte[] random = new byte[length];
            randomNumberGenerator.GetBytes(random);
            return random;
        }

        public static string GenerateBase64String(this RandomNumberGenerator randomNumberGenerator, int length)
        {
            byte[] random = new byte[length];
            randomNumberGenerator.GetBytes(random);
            return Convert.ToBase64String(random);
        }

        public static string GenerateHexString(this RandomNumberGenerator randomNumberGenerator, int length)
        {
            byte[] random = new byte[length];
            randomNumberGenerator.GetBytes(random);
            return BitConverter.ToString(random).Replace("-", "");
        }

        public static string ToQuery(this NameValueCollection nvc)
        {
            return String.Join("&", nvc.AllKeys.Select(a => a + "=" + WebUtility.UrlEncode(nvc[a])));
        }

        public static string Or(this string strVal, string orVal)
        {
            return strVal == null || strVal == string.Empty ? orVal : strVal;
        }

        public static string Hash(this string raw)
        {
            return raw.Hash(MD5.Create());
        }

        public static Database.Models.Team Team(this Database.Models.Member member)
        {
            return member.Database.Teams.Where(e => e.Id == member.TeamId).FirstOrDefault();
        }
        

        public static string Hash(this string raw, HashAlgorithm hashAlgorithm)
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(raw);
            byte[] hash = hashAlgorithm.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
                sb.Append(hash[i].ToString("x2"));

            return sb.ToString();
        }
    }

    public class ListTableAttribute : Attribute
    {
        public string Display { get; set; }
        public string TdClass { get; set; } = "";
        public string ThClass { get; set; } = "";
        public string ChildProperty { get; set; } = null;
        public string Format { get; set; } = "{0}";
        public int Position { get; set; } = 100;
        public bool Raw { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class IgnoreCopyAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class RateLimitAttribute : ActionFilterAttribute
    {
        public double Seconds { get; set; }
        public string View { get; set; } = "RateLimited";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.Controller.GetType().Name;
            string actionName = filterContext.ActionDescriptor.Name;
            var httpContext = filterContext.HttpContext;
            var connectionFeature = httpContext.GetFeature<IHttpConnectionFeature>();
            string ipAddress = connectionFeature.RemoteIpAddress.ToString();

            string requestIdentifier = $"Request-{controllerName}-{actionName}-{ipAddress}";

            bool allowRequest = false;

            if (Application.MemoryCache[requestIdentifier] == null)
            {
                DateTime expiration = DateTime.Now.AddSeconds(Seconds);
                Application.MemoryCache.Add(requestIdentifier, expiration, expiration);
                allowRequest = true;
            }

            if (!allowRequest)
            {
                DateTime expiration = (DateTime)Application.MemoryCache.Get(requestIdentifier);

                httpContext.Response.Headers.Add("Retry-After", new string[] { ((int)(expiration.ToUniversalTime() - DateTime.UtcNow).TotalSeconds).ToString() });
                filterContext.Result = new ViewResult
                {
                    StatusCode = 429,
                    ViewName = this.View
                };
            }
        }
    }
}

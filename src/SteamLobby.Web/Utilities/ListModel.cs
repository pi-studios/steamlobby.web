﻿using SteamLobby.Web.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteamLobby.Web
{
    public class ListModel
    {
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }

        public string EditLink { get; set; }
        public string DeleteLink { get; set; }
        public string CreateLink { get; set; }
        public IEnumerable<BaseModel> Items { get; set; }
    }
}
